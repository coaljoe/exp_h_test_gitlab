module h_test

go 1.18

require (
	github.com/bitly/go-simplejson v0.5.0
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20211024062804-40e447a793be
	github.com/google/go-jsonnet v0.18.0
	github.com/hjson/hjson-go v3.1.0+incompatible
	kristallos.ga/lib/debug v0.0.0-20210919214357-a61016f958a8
	kristallos.ga/rx v0.0.0-20220223112122-eaaeff6f0d76
	kristallos.ga/rx/math v0.0.0-20220223112122-eaaeff6f0d76
)

require (
	github.com/WastedCode/serializer v0.0.0-20150605061548-b76508a5f9d4 // indirect
	github.com/cheekybits/genny v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/fossoreslp/go-uuid-v4 v1.0.0 // indirect
	github.com/go-gl/gl v0.0.0-20211025173605-bda47ffaa784 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/iancoleman/orderedmap v0.2.0 // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	kristallos.ga/h_test/ulexite v0.0.0
	kristallos.ga/lib/ider v0.0.0-20210919214357-a61016f958a8 // indirect
	kristallos.ga/lib/pubsub v0.0.0-20210919214357-a61016f958a8 // indirect
	kristallos.ga/lib/sr v0.0.0-20210919214357-a61016f958a8 // indirect
	kristallos.ga/lib/xlog v0.0.0-20210919214357-a61016f958a8 // indirect
	kristallos.ga/rx/phys v0.0.0-20211030060450-7a579f5b884e // indirect
	kristallos.ga/rx/transform v0.0.0-20211030060450-7a579f5b884e // indirect
)

require (
	gopkg.in/yaml.v2 v2.2.7 // indirect
	sigs.k8s.io/yaml v1.1.0 // indirect
)

// XXX
replace kristallos.ga/h_test/ulexite => ./ulexite

//replace kristallos.ga/rx => ../../rx

//replace kristallos.ga/rx => ../../rx_upstream

//replace kristallos.ga/rx/math => ../../rx_upstream/math
//replace kristallos.ga/rx/transform => ../../rx_upstream/transform

// bb_local
// replace kristallos.ga/lib => ../../bb_local/lib
// //replace kristallos.ga/lib/sr => ../../bb_local/lib/sr
// replace kristallos.ga/lib/ider => ../../bb_local/lib/ider
// replace kristallos.ga/rx/math => ../../bb_local/rx/math
// //replace kristallos.ga/rx/transform => ./transform
// replace kristallos.ga/rx/transform => ../../bb_local/rx/transform
