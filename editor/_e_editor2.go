package main

func testEditor() {
	println("testEditor()")
}

func runEditor() {
	println("editor2.runEditor()")
	println("derp")

	e := newEditor()
	e.start()
	
	e_ctx.editor = e
	_editor = e
}

func init() {
	println("editor2.init")
	_runEditor = runEditor
	println("done editor2.init")
}

type Editor struct {

}

func newEditor() *Editor {
	println("newEditor()")
	
	e := &Editor{}

	println("done newEditor()")
	return e
}

func (e *Editor) start() {
	println("editor.start")

}

func (e *Editor) update(dt float64) {
	println("editor.update")
	pp(3)
}
