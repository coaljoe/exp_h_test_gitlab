package main

import (
	//"github.com/go-gl/glfw/v3.3/glfw"

	ul "kristallos.ga/h_test/ulexite"

	"kristallos.ga/rx"
	//"kristallos.ga/rx/plugins/manipulators"
	"fmt"
	//. "math"

	//. "kristallos.ga/rx/math"
)

//var rxi *rx.Rx

func main() {
	fmt.Println("main()")

	conf := rx.NewTestFwConf()
	conf.ResX = 1280
	conf.ResY = 800

	//app := rx.TestFwInit()
	app := rx.TestFwInitConf(conf)
	sce := rx.TestFwCreateDefaultScene()
	_ = sce


	uli := ul.NewUlexite()

	uli.InitEditor()

	// XXX editor tweaks
	gc := ul.Ctx.GameCamera
	_ = gc

	//gc.Camera.SetZoom(10)
	//pp(gc.Zoom())
	//gc.SetZoom(2)
	gc.SetZoom(4)
	

	for app.Step() {
		//println("step")
		dt := app.GetDt()


		uli.Update(dt)

		app.Flip()
	}

	println("exiting...")
}
