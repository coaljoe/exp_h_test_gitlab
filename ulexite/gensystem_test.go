package ulexite

import (
	"testing"
)

type TestEl struct {
	v int
}

func TestGenSystem(t *testing.T) {

	s := newGenSystem[*TestEl]()

	el1 := &TestEl{v: 1}
	el2 := &TestEl{v: 2}

	s.addElem(el1)
	s.addElem(el2)
	//s.addElem(el2)

	s.removeElem(el2)
	s.addElem(el2)
	//s.addElem(el2)
}
