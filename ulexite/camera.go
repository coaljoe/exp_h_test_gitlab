package ulexite

import (
	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"
)

type GameCamera struct {
	// XXX private ?
	camNode *rx.Node
	
	// Initial properties
	origRot Vec3
	origHeight float64
	origPos Vec3
	
	camRot Vec3
	camPos Vec3
	//camPoint Vec3 // Target
	distToCamPoint float64
}

func newGameCamera() *GameCamera {
	gc := &GameCamera{
	}
	
	return gc
}

func (gc *GameCamera) setup(camNode *rx.Node, rot Vec3, distToCamPoint float64) {
	p("GameCamera.setup camNode:", camNode, "rot:", rot, "distToCamPoint:", distToCamPoint)
	
	gc.camNode = camNode
	gc.origRot = rot
	gc.distToCamPoint = distToCamPoint
	
	t := rx.NewTransform()
	t.SetRot(gc.camNode.Rot())
	t.MoveByVec(Vec3{0, 0, distToCamPoint})
	
	gc.origPos = t.Pos()
	gc.origHeight = t.PosZ()
	p("->", t.Pos())
	
	newCamPos := gc.camNode.Pos()
	newCamPos.SetZ(gc.origHeight)
	p("newCamPos ->", newCamPos)
	
	gc.camPos = newCamPos
	gc.camRot = gc.origRot
	
	gc.build()
	
	//gc.panTo(Vec3{0, 0, 0})
}

func (gc *GameCamera) build() {
	p("GameCamera.build")

	p("camPos:", gc.camPos)
	p("camRot:", gc.camRot)
	gc.camNode.SetPos(gc.camPos)
	gc.camNode.SetRot(gc.camRot)
}

func (gc *GameCamera) camPoint() Vec3 {
	p("GameCamera.camPoint")
	
	panic("not implemented")
}

func (gc *GameCamera) Zoom() float64 {
	p("GameCamera Zoom")

	return gc.camNode.Camera.Zoom()
}

func (gc *GameCamera) SetZoom(v float64) {
	p("GameCamera SetZoom")
	
	gc.camNode.Camera.SetZoom(v)
}

func (gc *GameCamera) setFromCamPoint(point Vec3) {
	p("GameCamera.setFromCamPoint point:", point)
	
	t := rx.NewTransform()
	t.SetPos(point)
	t.SetRot(gc.camRot)
	t.MoveByVec(Vec3{0, 0, gc.distToCamPoint})
	
	newPos := t.Pos()

	p("newPos:", newPos)
	
	gc.camPos = newPos
	
	gc.build()
}

func (gc *GameCamera) pos() Vec3 {
	return gc.camPos
}

func (gc *GameCamera) rot() Vec3 {
	return gc.camRot
}

func (gc *GameCamera) height() float64 {
	return gc.camPos.Z()
}

func (gc *GameCamera) moveTo(pos Vec3) {
	p("GameCamera.moveTo pos:", pos)
	
	gc.camPos = pos
	gc.build()
}

// XXX rename to move
func (gc *GameCamera) panTo(pos Vec3) {
	p("GameCamera.panTo pos:", pos)
	
	gc.camPos = gc.camPos.Add(pos)
	gc.build()
}

// Focus/center camera on point
// XXX rename to panToCenter?
func (gc* GameCamera) panToPoint(centerPoint Vec3) {
	p("GameCamera.panToPoint centerPoint:", centerPoint)
	gc.setFromCamPoint(centerPoint)
	//gc.build()
}
