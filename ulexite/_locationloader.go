package main

type LocationLoader struct {

}

func newLocationLoader() *LocationLoader {
	l := &LocationLoader{}

	return l
}

func (l *LocationLoader) load(path string) {
	p("LocationLoader load path:", path)

	defPath := path + "/def.jsonnet"

	dat := unmarshalJsonnetFromFile(defPath)
	_ = dat

	p("done")
}
