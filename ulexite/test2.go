package ulexite

import (
	"kristallos.ga/rx"
	//"fmt"
	
	. "kristallos.ga/rx/math"
	
	"github.com/go-gl/glfw/v3.3/glfw"

	"math/rand"
)

func test2() TestUpdateFuncType {

	rxi := rx.Rxi()
	_ = rxi
	sce := rxi.Scene
	_ = sce
	
	sl := rx.NewSceneLoader()
	_ = sl
	
	makeCarScene := func() {

		carNode := sl.LoadSpawn("res1/objects/car1/car.dae")[0]
		carNode.SetName("_car")
		_ = carNode
		
		//pdump(sce.GetNodes())

		mat := carNode.Mesh.Material()
		mat.SetTwoSided(true)
		//mat.SetTwoSided(false)


		if false {
			mat.Texture().SetMagFilter(rx.TextureFilterNearest)
			mat.Texture().SetMinFilter(rx.TextureFilterNearest)
		
			minf := mat.Texture().MinFilter()
			magf := mat.Texture().MagFilter()
			p("minf:", minf)
			p("magf:", magf)

			mat.Texture().SetMipmaps(false)
			mips := mat.Texture().Mipmaps()
			p("mips:", mips)

			//mat.LoadTexture("res/models/primitives/box_textured/texture.png")
			
			//pp(2)
			//mat.SetOpt(
		}

		num := 3
		for i := 0; i < num; i++ {

			shX := float64(i)*5
			shY := float64(i)*0

			n2 := carNode.Clone()
			sce.Add(n2)
			//carNode.SetPos(Vec3{2, 2, 0})
			//carNode.SetPos(Vec3{0, -3, 0})
			n2.SetPos(Vec3{10 + shX, 1 + shY, 0})
		}
	}
	_ = makeCarScene
	
	makeCarScene()
	
	
	// SpriteMesh
	
	var sm *SpriteMesh
	sm = NewSpriteMesh()
	sm.W = 128*2
	sm.H = 128*2
	carNode := sce.GetNodeByName("_car#2") // fixme
	//carNode.SetPos(Vec3{3, 3, 0})
	//carNode.SetPos(Vec3{5, 5, 0})
	//carNode.SetPos(Vec3{5, 7, 0})
	//carNode.SetPos(Vec3{10, 10, 0})
	carNode.SetPos(Vec3{20, 20, 0})
	
	gc := Ctx.GameCamera
	_ = gc
	//gc.panToPoint(carNode.Pos())
	
	//pp(carNode.Pos())
	sm.SetMesh(carNode)
	sm.Spawn()
	//sm = nil
	
	if true {
	sms := Ctx.SpriteMeshSys
	sms.makeSpriteImage(sm)
	//sm.tex = sms.sceneRt.Framebuffer().Tex()
	//sm.tex = sms.procTexCrop
	//sm.shiftX = sms.procTexXCorr
	//sm.shiftY = sms.procTexYCorr
	sm.Build()
	sm.tex.Save("/tmp/2.png")
	
	
	// TODO record and apply yCorr (sprite Y-Correction in repect to center (yshift/offset))
	
	sx, sy, _ := rx.WorldToScreen(nil, carNode.Pos())
	newW := sm.tex.Width()
	newH := sm.tex.Height()
	sm.X = sx - newW / 2
	sm.Y = sy - newH / 2
	sm.X = sx
	sm.Y = sy
	sm.W = newW
	sm.H = newH
	
	ah := rx.NewAxesHelper()
	ah.SetPos(carNode.Pos())
	ah.NoDepthTest = true
	ah.Size = 100
	ah.Spawn()
	}

	// SM 2

	car2Node := carNode.Clone()
	car2Node.SetPos(Vec3{24, 16, 0})
	sce.Add(car2Node)

	sm2 := NewSpriteMesh()
	sm2.SetMesh(car2Node)
	sm2.Build()
	sm2.Spawn()
	
	rotated := false
	_ = rotated
	updateFunc := func(dt float64) {
		//pp(2)
		
				
		if sm != nil {
			sm.Render()
			sm.Update(dt)
		}
		
		{
			//if !rotated && rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeySpace) {
			if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeySpace) {
				v := rand.Float64() * 360.0
				//pp(carNode.Rot())
				carNode.SetRot(Vec3{90, 0, v})
				rotated = true
				
				sm.Build()
			}
		}
	}
	
	return updateFunc
}
