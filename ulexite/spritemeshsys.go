package ulexite

// TODO: use custom cameras instead of game cameras ?

import (
	"image"
	"math"
	//"image/draw"

	"kristallos.ga/rx"
	//. "kristallos.ga/rx/math"
	//"github.com/go-gl/gl/v2.1/gl"
)

type SpriteMeshSys struct {
	*GenSystem[*SpriteMesh]
	r              *rx.Renderer
	sceneRt        *rx.RenderTarget
	depthRt        *rx.RenderTarget
	procTex        *rx.Texture
	procTexImg     *image.NRGBA
	procTexImgCrop *image.NRGBA
	procTexCrop    *rx.Texture
	procTexXCorr   int
	procTexYCorr   int
}

func newSpriteMeshSys() *SpriteMeshSys {
	s := &SpriteMeshSys{
		GenSystem: newGenSystem[*SpriteMesh](),
	}

	return s
}

func (s *SpriteMeshSys) init() {
	s.r = rx.Rxi().Renderer
	width, height := s.r.Size()
	s.sceneRt = rx.NewRenderTarget("sceneRt", width, height, false) // for dynamic objects
	s.depthRt = rx.NewRenderTarget("depthRt", width, height, true)
}

func (s *SpriteMeshSys) add(el *SpriteMesh) {
	if s.has(el) {
		pp("already have elem: el:", el)
	}
	s.addElem(el)
}

func (s *SpriteMeshSys) remove(el *SpriteMesh) {
	if !s.has(el) {
		pp("don't have such elem: sm:", el)
	}
}

func (s *SpriteMeshSys) has(el *SpriteMesh) bool {
	return s.hasElem(el)
}

// XXX
func (s *SpriteMeshSys) GetProcTexImg() *image.NRGBA {
	return s.procTexImg
}

func (s *SpriteMeshSys) makeSpriteImage(sm *SpriteMesh) {
	// Focus on model
	// XXX should be first
	Ctx.GameCamera.panToPoint(sm.mesh.Pos())

	// Render dynamic objects to fbo
	//
	s.sceneRt.Bind()

	//gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	s.r.Clear()

	//cam := s.r.CurCam()
	cam := rx.Rxi().Camera
	s.r.SetCamera(cam)

	s.r.SetActiveNode(sm.mesh)
	//s.r.RenderAll()
	s.r.RenderScene()
	s.r.SetActiveNode(nil)

	//rp.depthRt.Unbind()
	s.sceneRt.Unbind()

	//// Set proc tex
	s.procTex = s.sceneRt.Framebuffer().Tex()
	s.procTexImg = s.procTex.GetTextureDataAsImage(true).(*image.NRGBA)
	//s.procTexImg = rx.

	s.procTex.Save("/tmp/ul__procTex.png")
	rx.SaveImage(s.procTexImg, "/tmp/ul__procTexImg.png", false)

	s.procData()
}

func (s *SpriteMeshSys) procData() {
	im := s.procTexImg
	w := im.Rect.Dx()
	h := im.Rect.Dy()

	// Find image extents
	minX, maxX := math.MaxInt32, 0
	minY, maxY := math.MaxInt32, 0
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//p("->", x, y)
			c := im.NRGBAAt(x, y)
			if c.A != 0 {
				if x < minX {
					minX = x
				}
				if x > maxX {
					maxX = x
				}
				if y < minY {
					minY = y
				}
				if y > maxY {
					maxY = y
				}
			}
		}
	}

	p("minX:", minX)
	p("maxX:", maxX)
	p("minY:", minY)
	p("maxY:", maxY)
	//pp(2)

	if maxX == 0 || maxY == 0 {
		// Empty image?
		pp("error: bad image: maxX:", maxX, "maxY:", maxY)
	}

	// Crop
	cropW := maxX - minX
	cropH := maxY - minY
	//cropIm := image.NewNRGBA(image.Rect(0, 0, cropW, cropH))
	cropIm := im.SubImage(image.Rect(minX, minY, maxX, maxY)).(*image.NRGBA)
	SaveImage(cropIm, "/tmp/x.png")

	s.procTexImgCrop = cropIm

	// Update yCorr
	yCorr := (h / 2) - minY
	//pp(yCorr)
	s.procTexYCorr = yCorr

	xCorr := (w / 2) - minX
	//pp(xCorr)
	s.procTexXCorr = xCorr

	p("xCorr:", xCorr)
	p("yCorr:", yCorr)

	/*
		b := cropIm.Bounds()
		rgba := image.NewRGBA(b)
		if rgba.Stride != rgba.Rect.Size().X*4 {
			panic("unsupported stride")
		}
		draw.Draw(rgba, rgba.Bounds(), cropIm, b.Min, draw.Src)
		saveImage(rgba, "/tmp/x1.png")

		data_raw := rgba.Pix
		_ = data_raw
	*/

	s.procTexCrop = rx.NewEmptyTexture(cropW, cropH, rx.TextureFormatRGBA)
	s.procTexCrop.SetTextureDataFromImage(cropIm, true)
}

// XXX fixme?
func (s *SpriteMeshSys) render() {
	panic("not implemented")
}

func (s *SpriteMeshSys) update(dt float64) {
	for _, sm := range s.elems {
		sm.Update(dt)
	}
}
