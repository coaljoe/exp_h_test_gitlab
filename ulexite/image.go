package ulexite

import (
	"fmt"
	"os"
	"image"
	"image/png"
)

func SaveImage(im image.Image, path string) {
	f, err := os.Create(path)
	if err != nil {
		fmt.Println("can't save image to path; path: ", path)
		panic(err)
	}
	defer f.Close()

	// disable compression
	enc := &png.Encoder{CompressionLevel: -1}
	enc.Encode(f, im)
}
