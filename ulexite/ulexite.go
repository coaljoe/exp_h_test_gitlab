package ulexite

import (
	"github.com/go-gl/glfw/v3.3/glfw"

	"kristallos.ga/rx"
	"kristallos.ga/rx/plugins/manipulators"
	"fmt"
	. "math"

	. "kristallos.ga/rx/math"
)

var rxi *rx.Rx
var gameField *Field

type Ulexite struct {
	// XXX fixme remove ?
	curTest TestUpdateFuncType
	
	// XXX
	node *rx.Node
	nodePos Vec3

	// Test ?
	// state ?
	v float64
	vInc float64
	rotate bool
}

func NewUlexite() *Ulexite {
	ul := &Ulexite{
		
	}

	return ul
}

// Common init routine ?
func (ul *Ulexite) baseInit(devMode bool, editorMode bool) {
	println("Ulexite baseInit()")

	println("devMode:", devMode)
	println("editorMode:", editorMode)
	
	rxi = rx.Rxi()
	rxi.Camera.Camera.SetZoom(2)

	sce := rxi.Scene
	//app := rxi.App

	
	
	cn := rxi.Camera
	if false {
		//cam.SetPos(Vec3{5, 5, 5})
		//cam.LookAt(Vec3Zero)
		cn.SetPos(Vec3{5, 10, 21.5})
		cn.SetRot(Vec3{12, 0, 11.8})
		cn.Camera.SetZoom(11.5 * 2)
	}
	//cn.Camera.SetFov(10)
	//cn.Camera.SetZoom(6)
	//cn.Camera.SetFov(20)
	cn.Camera.SetFov(40)
	cn.Camera.SetZoom(1)
	cn.Camera.SetZnear(0.01)
	cn.Camera.SetZfar(100)

	// Iso cam
	cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
	// Game iso 1:2
	cn.SetRot(Vec3{60, 0, 45})

	//fmt.Println(cn.Camera.Fov())
	//panic(2)
	cn.Camera.SetFov(10)


	gc := newGameCamera()
	Ctx.GameCamera = gc
	gc.setup(cn, Vec3{60 ,0, 45}, 40.0)
	
	gc.panTo(Vec3{10, 10})
	gc.panTo(Vec3{10, 10, 10})
	gc.panToPoint(Vec3{0, 0, 0})
	//pp(2)

	l1 := sce.GetNodeByName("light1")
	l1.SetPosZ(10)

	// XXX editor
	{
		grid := rx.NewGridHelper()
		grid.Width = 10
		grid.Height = 10
		grid.CellSize = 2.0
		grid.Color = Vec3{0.2, 0.3, 0.9}
		grid.DrawOrigin = true
		grid.Spawn()

		grid.SetPos(Vec3{0, 0, 0.02})
		//grid.SetPos(Vec3{0, 0, 1})
				
		// XXX
		//if !editorMode {
		if !devMode && !editorMode{
			grid.SetVisible(false)
		}
	}


	// XXX fixme ?
	//ctx.gameCamera.panToPoint(sm.mesh.Pos())
	//gc.panToPoint(Vec3Zero)
	//gc.panToPoint(Vec3{24, 16, 0})
	
	// XXX init base components ?
	// .. spritemesh is a part of it
	sms := newSpriteMeshSys()
	Ctx.SpriteMeshSys = sms
	sms.init()
}

// Empty init ?
func (ul *Ulexite) Init() {
	fmt.Println("Ulexite Init()")

	ul.baseInit(false, false)

}

// Empty init (dev version) ?
func (ul *Ulexite) InitDev() {
	fmt.Println("Ulexite InitDev()")

	ul.baseInit(true, false)

}

// Empty init (editor version) ?
// XXX add InitEditorDev ?
func (ul *Ulexite) InitEditor() {
	fmt.Println("Ulexite InitEditor()")

	ul.baseInit(true, true)

}

// Test init ?
func (ul *Ulexite) InitTest() {
	fmt.Println("Ulexite InitTest()")

	var _ = `
	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()
	_ = sce
	`
	rxi = rx.Rxi()
	rxi.Camera.Camera.SetZoom(2)

	sce := rxi.Scene
	//app := rxi.App

	cn := rxi.Camera
	_ = cn

	// XXX use Init() ?
	ul.baseInit(true, true)


	sl := rx.NewSceneLoader()
	_ = sl

	//nodes := sl.Load("res/models/primitives/triangle/triangle.dae")
	//nodes := sl.Load("res/models/primitives/square/square.dae")
	//nodes := sl.Load("tmp/square.dae")
	//nodes := sl.Load("res/models/primitives/box/box.dae")
	nodes := sl.Load("res/models/primitives/box_textured/box_textured.dae")
	//nodes := sl.Load("res/test/test_box/test_box_textured_numbers.dae")
	//nodes := sl.Load("tmp/test_uv/test_uv.dae")
	sl.Spawn()
	node := nodes[0]
	_ = node
	nodes = sl.Load("res/models/primitives/box/box.dae")

	// XXX
	ul.node = node
	
	sl.Spawn()
	rootNode := nodes[0]
	_ = rootNode

	
	ul.nodePos = Vec3Zero
	
	rootNode.SetName("rootNode")
	rootNode.SetPos(Vec3{3, 3, 0})
	
	// ?
	ul.nodePos = Vec3{3, 3, 0}
	
	
	
	//node.SetParent(rootNode)
	//node.Transform.SetParent(rootNode.Transform)
	
	if false {
	p := node.Parent()
	fmt.Println("p:", p)
	fmt.Println("p pos:", p.Pos())
	fmt.Println("pos:", node.Pos())
	fmt.Println("nodePos", node.Pos())
//	panic(2)
	}
	
	// SpriteMesh
	
	var _ = `
	sms := newSpriteMeshSys()
	Ctx.SpriteMeshSys = sms
	sms.init()
	`
	
	//test1()
	
	//var curTest TestUpdateFuncType
	//curTest = test1()
	ul.curTest = test2()

	//pp(2)

	if true {
		ms := manipulators.Init()
		_ = ms
		fmt.Println("ms:", ms)
		//panic(2)
	
		//m := rx.NewManipulator()
		//m := manipulators.NewManipulator()
		m := manipulators.NewManipulator(nil)
		_ = m
		m.Spawn()
		//sce.Add(m)

		//m.SetShowPickers(true)

		carNode := sce.GetNodeByName("_car#2") // fixme
		
		//m.Attach(node)
		m.Attach(carNode)
		//m.Detach()
	}
	
	
	//rootNode.SetRotZ(90)
	
	//rootNode.SetScale(Vec3{-1, 1, 1})
	
	//rootNode[0] = rootNode[0] * -1.0
	
	
	f := newField(10, 10)
	_ = f
	f.spawn()

	//ll := newLocationLoader()
	//ll.load("res1/locations/test_loc1")
	

	/*
	ah := rx.NewAxesHelper()
	ah.SetPos(Vec3{10, 10, 0})
	ah.Spawn()
	*/

	/*
		node := sl.Nodes()[0]
		node.SetPos(Vec3{4, 4, 0})
	*/


	ul.v = 0.0
	ul.vInc = 50.0
	ul.rotate = true
}

// XXX fixme ?
func (ul *Ulexite) baseUpdate(dt float64) {
	println("Ulexite baseUpdate")

	// XXX FIXME use context ?
	if Ctx.SpriteMeshSys != nil {
		Ctx.SpriteMeshSys.update(dt)
	}

	{
		//pp(c.cam.Pos(), c.cam.Rot())
		//moveSpeed := 10.0
		moveSpeed := 14.0
		//moveSpeed := 20.0
		shX := 0.0
		shY := 0.0
		_ = shX
		_ = shY
		k := 1.0
		//k := 0.5 // 1:2 iso
		if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyLeft) {
			shX = -moveSpeed
		}
		if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyRight) {
			shX = moveSpeed
		}
		if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyUp) {
			shY = moveSpeed * k
		}
		if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyDown) {
			shY = -moveSpeed * k
		}
			
		// XXX invY (according to world coords)
		//shY = -shY

		v := Vec3{shX * dt, shY * dt, 0}
		Ctx.GameCamera.panTo(v)
	}
}

func (ul *Ulexite) Update(dt float64) {
	println("Ulexite Update")

	ul.baseUpdate(dt)
}

func (ul *Ulexite) UpdateTest(dt float64) {
	println("Ulexite UpdateTest")

	ul.baseUpdate(dt)

	//app.
	cn := rxi.Camera

	{
		//println("step")

		//ctx.spriteMeshSys.update(dt)
		//ctx.spriteMeshSys.render(dt)
		
		// blue cube		
		cubePos := Vec3Zero
		_ = cubePos
		
		{
			c := cn
			fmt.Println(c.Camera.Fov())
			fmt.Println(c.Camera.Zoom())
			//panic(2)
			//println(c)
			ray := c.Camera.CreateRay()

			selRay := NewRay(ray.Origin, ray.Direction)
			// Do scene rayquery
			rc := rx.NewRaycaster()
			rc.TestRayScene(selRay)
			hitInfo := rc.TestRayGround(selRay)

			if hitInfo.Hit {
				//sce.GetNodeByName("box#2").SetPos(hitInfo.Position)
				cubePos = hitInfo.Position
			}
		
			d := rx.NewDrawer()

			//d.SetAt(1,1,1)
			d.SetAt(cubePos)
			d.SetColor(Vec3{0, 0, 1})
			d.DrawCube(1, Vec3{0, 0, 1})	
		}
		
		// get blue cube's world position
		cubePosWorld := viewToWorldPos(cubePos)
		_ = cubePosWorld
		
		
		{

			//pp(c.cam.Pos(), c.cam.Rot())
			moveSpeed := 10.0
			shX := 0.0
			shY := 0.0
			_ = shX
			_ = shY
			k := 1.0
			//k := 0.5 // 1:2 iso
			if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyLeft) {
				shX = -moveSpeed
			}
			if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyRight) {
				shX = moveSpeed
			}
			if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyUp) {
				shY = moveSpeed * k
			}
			if rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeyDown) {
				shY = -moveSpeed * k
			}
			
			// XXX invY (according to world coords)
			shY = -shY
			
			ul.nodePos = Vec3{ul.nodePos.X() + shX * dt, ul.nodePos.Y() + shY * dt, 0}
		}
		
		
		// Test
		ul.curTest(dt)

		
		nodePosView := worldToViewPos(ul.nodePos)
		
		//node.SetPos(nodePos)
		ul.node.SetPos(nodePosView)
		
		t := fmt.Sprintf("nodePos: %v\n", ul.nodePos)
		t += fmt.Sprintf("nodePosView: %v\n", nodePosView)
		t += fmt.Sprintf("node (real) pos: %v\n", ul.node.Pos())
		
		rx.DrawText(t, 0, 0, 20, Vec3{1, 1, 1})
		
		t2 := fmt.Sprintf("cubePos (view): %v\n", cubePos)
		t2 += fmt.Sprintf("cubeWorldPos (world): %v\n", cubePosWorld)
		
		rx.DrawText(t2, 0, 20*4, 13, Vec3{1, 1, 1})

		if ul.rotate {
			ul.v = Mod(ul.v+ul.vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			//sce.GetNodeByName("box").SetRot(Vec3{0, 0, v})
			//sce.GetNodeByName("triangle").SetRot(Vec3{v, 0, 0})
			//node.SetRot(Vec3{v, v, v})
			ul.node.SetRot(Vec3{ul.v, 0, ul.v})
		}

		// Update editor

		if _editor != nil {
			_editor.update(dt)
		}
	}
}
