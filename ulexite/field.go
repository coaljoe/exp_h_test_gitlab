package ulexite

import (
	"kristallos.ga/rx"
	.	"kristallos.ga/rx/math"
)

type Field struct {
	w, h int
	fieldOffsetPosY float64
	fieldOrigin Vec3
	view *FieldView
}

func newField(w, h int) *Field {
	f := &Field{
		w: w,
		h: h,
	}
	f.fieldOffsetPosY = float64(f.h) * float64(c_cell_size)
	f.fieldOrigin = Vec3{0, f.fieldOffsetPosY, 0}
	
	gameField = f
	return f
}

func (f *Field) spawn() {
	f.view = newFieldView(f)
	f.view.spawn()
}

// Map field's x, y coord to view coord
// getPositionAt
func (f *Field) getViewPositionAt(worldX, worldY float64) (float64, float64) {
	return worldToView(worldX, worldY)
}


type FieldView struct {
	m *Field
	spawned bool
}

func newFieldView(m *Field) *FieldView {
	v := &FieldView{
		m: m,
	}
	return v
}

func (v *FieldView) spawn() {

	/*
	//n := rx.NewNode(rx.NodeType_Empty)
	n := rx.NewEmptyNode()
	//n.Spawn()
	rxi.Scene().Add(n)
	*/
	
	n := rx.NewAxesHelper()
	n.Spawn()
	
	n.SetPos(v.m.fieldOrigin)

	v.spawned = true
}
