module kristallos.ga/h_test/hte

go 1.18

require (
	github.com/bitly/go-simplejson v0.5.0
	github.com/google/go-jsonnet v0.18.0
	github.com/hjson/hjson-go v0.2.3
	kristallos.ga/lib/debug v0.0.0-20210919214357-a61016f958a8
	kristallos.ga/rx v0.0.0-20220223112122-eaaeff6f0d76
	kristallos.ga/rx/math v0.0.0-20220223112122-eaaeff6f0d76
)

require (
	github.com/WastedCode/serializer v0.0.0-20150605061548-b76508a5f9d4 // indirect
	github.com/cheekybits/genny v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/fossoreslp/go-uuid-v4 v1.0.0 // indirect
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20200707082815-5321531c36a2 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
	kristallos.ga/lib/ider v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/pubsub v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/sr v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/xlog v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/rx/phys v0.0.0-20210204202831-c0aefa214cc6 // indirect
	kristallos.ga/rx/transform v0.0.0-20210204202004-42f498edb097 // indirect
	sigs.k8s.io/yaml v1.1.0 // indirect
)
