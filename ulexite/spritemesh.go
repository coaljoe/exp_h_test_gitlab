package ulexite

import (
	"kristallos.ga/rx"
	"fmt"

	//. "kristallos.ga/rx/math"
)

type SpriteMesh struct {
	W int
	H int
	X int
	Y int
	// xCorr
	ShiftX int
	// yCorr
	ShiftY int
	mesh *rx.Node
	tex *rx.Texture
	spawned bool
	d_gen int
}

func NewSpriteMesh() *SpriteMesh {
	sm := &SpriteMesh{
		W: 128,
		H: 128,
	}
	
	return sm
}

func (sm *SpriteMesh) SetMesh(m *rx.Node) {
	sm.mesh = m
}

func (sm *SpriteMesh) SaveTex(path string) {
	sm.tex.Save(path)
}

func (sm *SpriteMesh) Spawn() {
	fmt.Println("Spritemesh Spawn")
	sm.spawned = true
}

func (sm *SpriteMesh) Build() {
	sms := Ctx.SpriteMeshSys
	
	sms.makeSpriteImage(sm)
	//sm.tex = sms.sceneRt.Framebuffer().Tex()
	sm.tex = sms.procTexCrop
	
	sm.W = sm.tex.Width()
	sm.H = sm.tex.Height()
	// XXX set x and y too here?
	
	
	sm.ShiftX = sms.procTexXCorr
	sm.ShiftY = sms.procTexYCorr
	
	sm.d_gen++
	
	if sm.d_gen > 1 {
		//pp(2)
	}
}

func (sm *SpriteMesh) Render() {	
	d := rx.NewDrawer()
	//d.DrawRect(sm.x, sm.y, sm.w, sm.h, d.ColorWhite)
	//d.DrawRectTex(sm.x, sm.y, sm.w, sm.h, d.ColorWhite, sm.tex)
	//d.DrawRectTex(sm.x - sm.w/2, sm.y - sm.shiftY, sm.w, sm.h, d.ColorWhite, sm.tex)
	d.DrawRectTex(sm.X - sm.ShiftX, sm.Y - sm.ShiftY, sm.W, sm.H, d.ColorWhite, sm.tex)
}

func (sm *SpriteMesh) Update(dt float64) {

}
