// Generic System
package ulexite

type GenSystem[T comparable] struct {
	elems []T
}

func newGenSystem[T comparable]() *GenSystem[T] {
	s := &GenSystem[T]{}

	return s
}

func (s *GenSystem[T]) idxElem(el T) int {
	for i, x := range s.elems {
		if x == el {
			return i
		}
	}
	return -1
}

func (s *GenSystem[T]) hasElem(el T) bool {
	return s.idxElem(el) != -1
}

func (s *GenSystem[T]) addElem(el T) {
	if s.hasElem(el) {
		pp("GenSystem: already have this elem:", el)
	}
	s.elems = append(s.elems, el)
}

func (s *GenSystem[T]) removeElem(el T) {
	if !s.hasElem(el) {
		pp("GenSystem: cannot remove elem, not found:", el)
	}
	idx := s.idxElem(el)
	// XXX remove
	s.elems = append(s.elems[:idx], s.elems[idx+1:]...)
}
