package ulexite

import (
	"kristallos.ga/rx"
	//"fmt"
	
	. "kristallos.ga/rx/math"
	
	"github.com/go-gl/glfw/v3.3/glfw"

	"math/rand"
)

func test1() TestUpdateFuncType {

	rxi := rx.Rxi()
	_ = rxi
	sce := rxi.Scene
	_ = sce
	
	sl := rx.NewSceneLoader()
	_ = sl
	
	makeCarScene := func() {

		carNode := sl.LoadSpawn("res1/objects/car1/car.dae")[0]
		carNode.SetName("_car")
		_ = carNode
		
		//pdump(sce.GetNodes())

		mat := carNode.Mesh.Material()
		mat.SetTwoSided(true)
		//mat.SetTwoSided(false)

		num := 3
		for i := 0; i < num; i++ {

			shX := float64(i)*5
			shY := float64(i)*0

			n2 := carNode.Clone()
			sce.Add(n2)
			//carNode.SetPos(Vec3{2, 2, 0})
			//carNode.SetPos(Vec3{0, -3, 0})
			n2.SetPos(Vec3{10 + shX, 1 + shY, 0})
		}
	}
	_ = makeCarScene
	
	makeCarScene()
	
	
	// SpriteMesh
	
	var sm *SpriteMesh
	sm = NewSpriteMesh()
	sm.W = 128*2
	sm.H = 128*2
	carNode := sce.GetNodeByName("_car#2") // fixme
	//pp(carNode.Pos())
	sm.SetMesh(carNode)
	sm.Spawn()
	//sm = nil
	
	sms := Ctx.SpriteMeshSys
	sms.makeSpriteImage(sm)
	//sm.tex = sms.sceneRt.Framebuffer().Tex()
	//sm.tex = sms.procTexCrop
	//sm.shiftX = sms.procTexXCorr
	//sm.shiftY = sms.procTexYCorr
	sm.Build()
	sm.tex.Save("/tmp/2.png")
	
	// TODO record and apply yCorr (sprite Y-Correction in repect to center (yshift/offset))
	
	sx, sy, _ := rx.WorldToScreen(nil, carNode.Pos())
	newW := sm.tex.Width()
	newH := sm.tex.Height()
	sm.X = sx - newW / 2
	sm.Y = sy - newH / 2
	sm.X = sx
	sm.Y = sy
	sm.W = newW
	sm.H = newH
	
	ah := rx.NewAxesHelper()
	ah.SetPos(carNode.Pos())
	ah.NoDepthTest = true
	ah.Size = 100
	ah.Spawn()
	
	rotated := false
	updateFunc := func(dt float64) {
		//pp(2)
		
		{
			if !rotated && rxi.InputSys.Keyboard.IsKeyPressed(glfw.KeySpace) {
				v := rand.Float64() * 360.0
				//pp(carNode.Rot())
				carNode.SetRot(Vec3{90, 0, v})
				rotated = true
				
				sm.Build()
			}
		}
	}
	
	return updateFunc
}
