package ulexite

import (
	. "kristallos.ga/rx/math"
)


// Convert world coords to view coords
func worldToView(x, y float64) (float64, float64) {
	viewY := float64(gameField.h) - y // invY
	return x, viewY
}

func worldToViewPos(p Vec3) Vec3 {
	viewX, viewY := worldToView(p[0], p[1])
	return Vec3{viewX, viewY, p[2]}
}

// Convert view coords to world coords
func viewToWorld(x, y float64) (float64, float64) {
	worldY := y - float64(gameField.h) // invY
	return x, worldY
}

func viewToWorldPos(p Vec3) Vec3 {
	worldX, worldY := worldToView(p[0], p[1])
	return Vec3{worldX, worldY, p[2]}
}
