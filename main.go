package main

import (
	//"github.com/go-gl/glfw/v3.3/glfw"

	ul "kristallos.ga/h_test/ulexite"

	"kristallos.ga/rx"
	//"kristallos.ga/rx/plugins/manipulators"
	"fmt"
	//. "math"

	//. "kristallos.ga/rx/math"
)

//var rxi *rx.Rx

func main() {
	fmt.Println("main()")

	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()
	_ = sce


	uli := ul.NewUlexite()

	uli.InitTest()

	for app.Step() {
		//println("step")
		dt := app.GetDt()


		//htei.Update(dt)
		uli.UpdateTest(dt)

		app.Flip()
	}

	println("exiting...")
}
