package main

import (
	//"github.com/go-gl/glfw/v3.3/glfw"

	ul "kristallos.ga/h_test/ulexite"

	"kristallos.ga/rx"
	//"kristallos.ga/rx/plugins/manipulators"
	"fmt"
	//. "math"
	"image"

	//. "kristallos.ga/rx/math"
)

//var rxi *rx.Rx

func main() {
	fmt.Println("main()")

	conf := rx.NewTestFwConf()
	conf.ResX = 1280
	conf.ResY = 800
	

	//app := rx.TestFwInit()
	app := rx.TestFwInitConf(conf)
	sce := rx.TestFwCreateDefaultScene()
	_ = sce
	
	// XXX disable debug ?
	xconf := rx.GetConf()
	_ = xconf
	//xconf.DebugDraw = false

	uli := ul.NewUlexite()

	uli.Init()
	//uli.InitDev()

	// XXX editor tweaks
	gc := ul.Ctx.GameCamera
	_ = gc

	//gc.Camera.SetZoom(10)
	//pp(gc.Zoom())
	//gc.SetZoom(2)
	gc.SetZoom(4)
	
	// XXX lighting ?
	
	
	rxi := rx.Rxi()
	_ = rxi
	//sce := rxi.Scene
	//_ = sce
	
	sl := rx.NewSceneLoader()
	_ = sl
	
	objNode := sl.LoadSpawn("res1/objects/car1/car.dae")[0]
	//carNode.SetName("_car")
	//_ = carNode
	
	mat := objNode.Mesh.Material()
	mat.SetTwoSided(true)
	
	sce.Add(objNode)
		
	//pdump(sce.GetNodes())
	
	// Opts ?
	fromAngle := 0
	toAngle := 360
	//angleStep := 0.5
	angleStep := 1.0
	
	imResX := 256
	imResY := 256

	makeShot := func(outpath string) {
	
		sm2 := ul.NewSpriteMesh()
		sm2.SetMesh(objNode)
		sm2.Build()
		sm2.Spawn()
	
		sms := ul.Ctx.SpriteMeshSys
		im1 := sms.GetProcTexImg()
	
		// Crop
		im1W := im1.Rect.Dx()
		im1H := im1.Rect.Dy()
		x0 := im1W/2 - imResX/2
		y0 := im1H/2 - imResY/2
		x1 := im1W/2 + imResX/2
		y1 := im1H/2 + imResY/2
		cropIm := im1.SubImage(image.Rect(x0, y0, x1, y1)).(*image.NRGBA)
		ul.SaveImage(im1, "/tmp/x1.png")
		ul.SaveImage(cropIm, "/tmp/x1_crop.png")
		ul.SaveImage(cropIm, outpath)
	
		//pdump(sm2)
	
		sm2.SaveTex("/tmp/out.png")
	
	}
	
	makeShot("/tmp/q.png")
	
	numSteps := int((float64(toAngle) - float64(fromAngle)) / angleStep)
	curAngle := 0.0
	for i := 0; i < numSteps; i++ {
		fmt.Println("->", i)

		curAngle += angleStep
		fmt.Println("curAngle:", curAngle)
		
		outPath := fmt.Sprintf("/tmp/xout2/%04d.png", i)
		fmt.Println("outPath:", outPath)
		
		
		objNode.SetRotZ(curAngle)

		makeShot(outPath)
	}
	
	pp(2)

	for app.Step() {
		//println("step")
		dt := app.GetDt()


		uli.Update(dt)

		app.Flip()
	}

	println("exiting...")
}
