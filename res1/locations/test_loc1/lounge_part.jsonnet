{
	// XXX should this part have two chunks?
	// 1st - lounge part chunk
	// 2nd - glass windows on the perimeter chunk?
	
	id: "lounge_part",
	name: "Lounge part",
	description: "Lounge part of the building, 2nd floor",
	size: [10, 10],
	pos: [10, 10, 0],
	floor_level: 1,
	links: ["hall_part"],
	//environment_name: 'default_inside',
	environment: {
		name: "Lounge",
		temperature: 15,
		sound: "hall_bg.ogg",
		music: "mus_mall_bg.ogg",
	},
}
