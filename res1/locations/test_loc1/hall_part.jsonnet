{
	scene: "hall_part_scene.jsonnet",

	id: "hall_part",
	name: "Test hall part",
	description: """
		Test hall part of the location
	""",
	size: [10, 20], // Tiles/meters?
	pos: [0, 0, 0],
	floor_level: 0,
	linked_parts: [""],
	environment_name: "default_inside",
	// Default items of an uninitialized location,
	// later replaced by actual saved list
	items: [
		{
			typename: "G17",
			position: [0, 0, 0],
		},
		{
			typename: "boots",
			position: [1, 1, 0],
		},
	],
	objects: [
		{
			id: "door",
			model: "door.dae",
			// Editor description?
			description: "Left-side wide door to the stairs/elevators?",
			position: [0, 5],
			useable: true,
			type: "portal", // Portal: cant be used by monsters (and most npcs)?
		},
		{
			id: "door2",
			model: "door2.dae",
			description: "Front-side wide main door",
			position: [10, 0],
			useable: true,
			type: "link", // Short link to another chunk (?)
		},
	],
}
