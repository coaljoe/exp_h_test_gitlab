//local test_chunk = import "test_chunk.jsonnet";

{
	// Location info
	location_info: {
		id: "test_rooms",
		name: "Test Reference",
		// Number of elevations?
		num_floor_levels: 3,
		// 1st: hall
		// 2nd: lounge
		// 3rd: roof?

		// Entry points
		entry_points: [
			{
				//name: "Hall",
				custom_name: "Hall",
				// Chunk ids
				//chunks: "test_chunk", // XXX rename chunks
				part: "hall_part",
				// XXX add default/start chunk?
			},
			{
				// XXX use default name?
				//name: "Lounge",
				part: "lounge_part",
			},
		],
	},

	location_parts: [
		{
			id: "hall_part",
			name: "Hall",
			// 1st floor
		},
		{
			id: "lounge_part",
			name: "Lounge",
			// 2nd floor
			// XXX todo: add floor/z-level prefixes? like l1_lounge?
		},
		{
			id: "roof_part",
			name: "Roof",
			// 3rd floor (technical)
			// XXX remove/make/mark as unaccessible?
			access: false,
		},
	]
}
